<?php

namespace Models;

use App\Models\Company;
use App\Models\User;
use Laravel\Lumen\Testing\DatabaseMigrations;

class CompanyTest extends \TestCase
{
    use DatabaseMigrations;

    public function testUser()
    {
        $user = User::factory()
            ->count(1)
            ->create()
            ->first();
        $company = Company::factory()
            ->count(5)
            ->create(['user_id' => $user->id])
            ->first();
        $this->assertEquals($user->email, $company->user->email);
    }
}
