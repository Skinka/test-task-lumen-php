<?php

namespace App\DTO;

class NewCompanyDTO extends DTO
{
    public string  $name;
    public ?string $phone;
    public ?string $description;
}