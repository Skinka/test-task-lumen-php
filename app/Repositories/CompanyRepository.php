<?php

namespace App\Repositories;

use App\DTO\NewCompanyDTO;
use App\Models\Company;
use App\Models\Company as Model;
use Illuminate\Database\Eloquent\Collection;

class CompanyRepository extends Repository
{
    public function getByUser(int $userId): Collection
    {
        return $this->startConditions()->where('user_id', $userId)->get();
    }

    /**
     * @throws \Throwable
     */
    public function create(int $userId, NewCompanyDTO $dto): ?Model
    {
        $model = new Company($dto->toArray());
        $model->user_id = $userId;
        $model->saveOrFail();
        return $model;
    }

    protected function getModelClass(): string
    {
        return Model::class;
    }
}