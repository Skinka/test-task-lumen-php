<?php

namespace Services;

use App\Models\User;
use App\Services\AuthService;
use Illuminate\Http\Request;
use Laravel\Lumen\Testing\DatabaseMigrations;

class AuthServiceTest extends \TestCase
{
    use DatabaseMigrations;

    public function testGetUserForAuth()
    {
        /** @var AuthService $service */
        $service = app(AuthService::class);
        $user = User::factory()
            ->count(1)
            ->create()
            ->first();
        $user = $service->generateApiToken($user);
        $this->assertNotNull($user->auth_token);
        $request = new Request();
        $request->headers->set('Authorization', "Bearer {$user->auth_token}");
        $userByToken = $service->getUserForAuth($request);
        $this->assertEquals($user->auth_token, $userByToken->auth_token);
        $this->assertEquals($user->email, $userByToken->email);
    }
}
