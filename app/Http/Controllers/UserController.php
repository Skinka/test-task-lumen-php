<?php

namespace App\Http\Controllers;

use App\DTO\NewCompanyDTO;
use App\Http\Resources\CompanyResource;
use App\Repositories\CompanyRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    private CompanyRepository $companyRepository;

    public function __construct(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }

    /**
     * Get list companies by auth user
     *
     * @return JsonResponse
     */
    public function companies(): JsonResponse
    {
        $companies = $this->companyRepository->getByUser(Auth::id());
        return response()->json(CompanyResource::collection($companies));
    }

    /**
     * Attach new company for auth user
     *
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Spatie\DataTransferObject\Exceptions\UnknownProperties
     * @throws \Throwable
     */
    public function newCompany(Request $request): JsonResponse
    {
        $this->validate($request, [
            'name'        => 'required|string',
            'phone'       => 'nullable|string',
            'description' => 'nullable|string'
        ]);

        $dto = new NewCompanyDTO($request->only(['name', 'phone', 'description']));
        $model = $this->companyRepository->create(Auth::id(), $dto);
        return response()->json(new CompanyResource($model), Response::HTTP_CREATED);
    }
}