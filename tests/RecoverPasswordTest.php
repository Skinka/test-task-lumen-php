<?php

use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Laravel\Lumen\Testing\DatabaseMigrations;

class RecoverPasswordTest extends TestCase
{
    use DatabaseMigrations;

    public function test_get_token_success()
    {
        User::factory()
            ->count(1)
            ->create(['email' => 'test@test.com'])
            ->first();
        $this->patch('/api/user/recover-password', ['email' => 'test@test.com'])
            ->seeStatusCode(Response::HTTP_NO_CONTENT);
    }

    public function test_get_token_validate()
    {
        $this->patch('/api/user/recover-password', ['email' => 'test1@test.com'])
            ->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->seeJsonEquals([
                "email" => [
                    "The selected email is invalid."
                ],
            ]);

        $this->patch('/api/user/recover-password', ['email' => 'test2test.com'])
            ->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->seeJsonEquals([
                "email" => [
                    "The email must be a valid email address."
                ],
            ]);
    }

    public function test_recover_password_success()
    {
        $token = Str::random(32);
        $user = User::factory()
            ->count(1)
            ->create([
                'password_reset_token' => $token
            ])
            ->first();

        $this->post('/api/user/recover-password', [
            'email'                 => $user->email,
            'token'                 => $token,
            'password'              => '1234567',
            'password_confirmation' => '1234567'
        ])
            ->seeStatusCode(Response::HTTP_NO_CONTENT);
    }

    public function test_recover_password_validate()
    {
        $token = Str::random(32);
        $user = User::factory()
            ->count(1)
            ->create([
                'password_reset_token' => $token
            ])
            ->first();

        $this->post('/api/user/recover-password', [
            'email'                 => $user->email,
            'token'                 => '1234',
            'password'              => '1234567',
            'password_confirmation' => '1234567'
        ])
            ->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->seeJsonEquals(['token' => ['Token invalid.']]);

        $this->post('/api/user/recover-password', [
            'email'                 => $user->email,
            'token'                 => null,
            'password'              => '1234567',
            'password_confirmation' => '1234567'
        ])
            ->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->seeJsonEquals(['token' => ['The token field is required.']]);

        $this->post('/api/user/recover-password', [
            'email'                 => $user->email,
            'token'                 => $token,
            'password'              => '123456',
            'password_confirmation' => '1234567'
        ])
            ->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->seeJsonEquals(['password' => ['The password confirmation does not match.']]);

        $this->post('/api/user/recover-password', [
            'email'                 => $user->email,
            'token'                 => $token,
            'password'              => '',
            'password_confirmation' => ''
        ])
            ->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->seeJsonEquals([
                'password'              => ['The password field is required.'],
                'password_confirmation' => ['The password confirmation field is required.']
            ]);

        $this->post('/api/user/recover-password', [
            'email'                 => 'test@test.com',
            'token'                 => $token,
            'password'              => '123',
            'password_confirmation' => '123'
        ])
            ->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->seeJsonEquals(['email' => ['The selected email is invalid.']]);
    }
}
