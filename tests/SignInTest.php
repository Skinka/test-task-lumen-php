<?php

use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Testing\DatabaseMigrations;

class SignInTest extends TestCase
{
    use DatabaseMigrations;

    public function test_success()
    {
        /** @var User $user */
        $user = User::factory()
            ->count(1)
            ->create(['password' => Hash::make('123')])
            ->first();
        $this->post('/api/user/sign-in', ['email' => $user->email, 'password' => '123'])
            ->seeStatusCode(Response::HTTP_OK)
            ->seeJsonStructure(['token']);
    }

    public function test_validate()
    {
        /** @var User $user */
        $user = User::factory()
            ->count(1)
            ->create(['password' => Hash::make('123')])
            ->first();

        $this->post('/api/user/sign-in', [])
            ->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->seeJsonEquals([
                "email"    => [
                    "The email field is required."
                ],
                "password" => [
                    "The password field is required."
                ]
            ]);
        $this->post('/api/user/sign-in', ['email' => 'email@email.com', 'password' => '123'])
            ->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->seeJsonEquals([
                "email" => [
                    "The selected email is invalid."
                ],
            ]);
        $this->post('/api/user/sign-in', ['email' => 'email2email.com', 'password' => '123'])
            ->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->seeJsonEquals([
                "email" => [
                    "The email must be a valid email address."
                ],
            ]);
        $this->post('/api/user/sign-in', ['email' => $user->email, 'password' => '123456'])
            ->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->seeJsonEquals([
                "password" => [
                    "The password field is incorrect."
                ]
            ]);
    }
}
