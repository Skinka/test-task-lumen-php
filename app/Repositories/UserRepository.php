<?php

namespace App\Repositories;

use App\DTO\RegisterDTO;
use App\Models\User;
use App\Models\User as Model;

class UserRepository extends Repository
{
    /**
     * Get User by email
     *
     * @param  string  $email
     *
     * @return Model|null
     */
    public function getByEmail(string $email): ?Model
    {
        return $this->startConditions()->where('email', strtolower($email))->firstOrFail();
    }

    /**
     * Get User by auth token
     *
     * @param  string  $token
     *
     * @return Model|null
     */
    public function getByAuthToken(string $token): ?Model
    {
        return $this->startConditions()->where('auth_token', $token)->first();
    }

    public function authTokenExist(string $token): bool
    {
        return $this->startConditions()->where('auth_token', $token)->exists();
    }

    /**
     * @throws \Throwable
     */
    public function create(RegisterDTO $dto): Model
    {
        $model = new User();
        $model->email = strtolower($dto->email);
        $model->last_name = $dto->last_name;
        $model->first_name = $dto->first_name;
        $model->phone = $dto->phone;
        $model->setPassword($dto->password);
        $model->saveOrFail();
        return $model;
    }

    /**
     * @inheritDoc
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }
}