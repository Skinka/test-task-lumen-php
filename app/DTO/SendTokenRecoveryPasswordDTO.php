<?php

namespace App\DTO;

class SendTokenRecoveryPasswordDTO extends DTO
{
    public string $email;
}