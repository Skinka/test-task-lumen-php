<?php

namespace App\DTO;

class RegisterDTO extends DTO
{
    public string  $email;
    public string  $password;
    public ?string $first_name;
    public ?string $last_name;
    public ?string $phone;
}