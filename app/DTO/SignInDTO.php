<?php

namespace App\DTO;

class SignInDTO extends DTO
{
    public string $email;
    public string $password;
}