# Test task Lumen PHP Framework

## Task

### Stack:

- Lumen

- PostgreSQL

### Task:

Create RESTFull API.

### Description:

Create the API to share the company's information for the logged users.

### Details:

Create DB migrations for the tables: users, companies, etc. Suggest the DB structure. Fill the DB with the test data.

### Endpoints:

- https://domain.com/api/user/register
  — method POST — fields: first_name [string], last_name [string], email [string], password [string], phone [string]

- https://domain.com/api/user/sign-in
  — method POST — fields: email [string], password [string]

- https://domain.com/api/user/recover-password
  — method POST/PATCH — fields: email [string] // allow to update the password via email token

- https://domain.com/api/user/companies
  — method GET — fields: title [string], phone [string], description [string]
  — show the companies, associated with the user (by the relation)

- https://domain.com/api/user/companies
  — method POST — fields: title [string], phone [string], description [string]
  — add the companies, associated with the user (by the relation)

Unit tests are optional.

## Develop

### Install

```
make install
```

### Test

```
make test
```

### Run

Open in browser `http://localhost`

## API

### Sign In

Request

```
POST /api/user/sign-in
Host: localhost
Accept: application/json

email=email%40email.com&password=123456
```

Response (200)

```
{
  "token": "auth token"
}
```

### Register

Request

```
POST /api/user/register
Host: localhost
Accept: application/json

email=test%40tes1t.com&password=123456&first_name=Roman&last_name=Roman&phone=%2B380956263577
```

Response (201)

```
{
    "id": 13,
    "email": "test@tes1t.com",
    "first_name": "Roman",
    "last_name": "Roman",
    "phone": "+380956263577"
}
```

### Companies list by auth user

Request

```
GET /api/user/companies
Host: localhost
Authorization: Bearer AUTH_TOKEN
Accept: application/json
```

Response (200)

```
[
    {
        "id": 7,
        "name": "Ritchie Ltd",
        "phone": "+14786987222",
        "description": "In another moment that it is!"
    },
    {
        "id": 45,
        "name": "Price, Keeling and Torp",
        "phone": "+13128783496",
        "description": "Dormouse crossed the court, arm-in-arm with the edge of her going."
    }
]
```

### Create Company and attach to auth user

Request

```
POST /api/user/companies
Host: localhost
Authorization: Bearer AUTH_TOKEN
Accept: application/json

name=Roga&phone=%2B380956263577&description=123
```

Response (201)

```
{
    "id": 101,
    "name": "Roga",
    "phone": "+380956263577",
    "description": "123"
}
```

## Validation error (422)

```
{
    "field": [
        "Error text.",
        "Error text."
    ],
    "field2": [
        "Error text.",
    ]
}
```