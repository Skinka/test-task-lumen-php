<?php

use Illuminate\Http\Response;
use Laravel\Lumen\Testing\DatabaseMigrations;

class RegisterTest extends TestCase
{
    use DatabaseMigrations;

    public function test_success()
    {
        $this->post('/api/user/register', [
            'email'      => 'test@test.com',
            'password'   => '123',
            'first_name' => 'Test',
            'last_name'  => 'Test',
            'phone'      => '+380991234567'
        ])
            ->seeStatusCode(Response::HTTP_CREATED)
            ->seeJsonEquals([
                'id'         => 1,
                'email'      => 'test@test.com',
                'first_name' => 'Test',
                'last_name'  => 'Test',
                'phone'      => '+380991234567'
            ]);
    }

    public function test_validate()
    {
        $this->post('/api/user/register', [
            'email'      => 'test2test.com',
            'password'   => '123',
            'first_name' => 'Test',
            'last_name'  => 'Test',
            'phone'      => '+380991234567'
        ])
            ->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->seeJsonEquals([
                "email" => [
                    "The email must be a valid email address."
                ],
            ]);
        $this->post('/api/user/register', [
            'email'      => 'test@test.com',
            'password'   => '',
            'first_name' => 'Test',
            'last_name'  => 'Test',
            'phone'      => '+380991234567'
        ])
            ->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->seeJsonEquals([
                "password" => [
                    "The password field is required."
                ],
            ]);

        $this->post('/api/user/register', [
            'email'      => 'test@test.com',
            'password'   => '123',
            'first_name' => 'Test',
            'last_name'  => 'Test',
            'phone'      => '+380991234567'
        ])->post('/api/user/register', [
            'email'      => 'test@test.com',
            'password'   => '123',
            'first_name' => 'Test',
            'last_name'  => 'Test',
            'phone'      => '+380991234567'
        ])->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->seeJsonEquals([
                "email" => [
                    "The email has already been taken."
                ],
            ]);
    }
}
