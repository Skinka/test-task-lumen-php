default:
	run

# Start the containers
run:
	docker-compose -p skinka-test-lumen up -d

# Stop the containers
stop:
	docker-compose -p skinka-test-lumen down

# Install environments and run project
install:
	docker-compose -p skinka-test-lumen up -d --build
	docker-compose -p skinka-test-lumen exec php composer install --no-interaction --ansi
	docker-compose -p skinka-test-lumen exec php php -r "file_exists('.env') || copy('.env.example', '.env');"
	docker-compose -p skinka-test-lumen exec php php artisan migrate --seed

# Test
test:
	docker-compose -p skinka-test-lumen exec php vendor/bin/phpunit --coverage-text --colors=never

