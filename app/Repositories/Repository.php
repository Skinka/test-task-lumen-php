<?php


namespace App\Repositories;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Repository
 * @package App\Repositories
 */
abstract class Repository
{
    /** @var Model */
    protected mixed $model;

    /**
     * CoreRepository constructor.
     */
    public function __construct()
    {
        $this->model = app($this->getModelClass());
    }


    /**
     * Return model class name
     *
     * @return string
     */
    abstract protected function getModelClass(): string;


    /**
     * Start query from model
     *
     * @return Model|Builder
     */
    protected function startConditions(): Model|Builder
    {
        return clone $this->model;
    }

    /**
     * Get data by id
     *
     * @param $id
     *
     * @return Model|null
     */
    public function getById($id): ?Model
    {
        return $this->startConditions()->where('id', $id)->firstOrFail();
    }
}
