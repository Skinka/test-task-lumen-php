<?php

namespace App\Services;

use App\DTO\RecoveryPasswordDTO;
use App\DTO\SendTokenRecoveryPasswordDTO;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthService
{
    private UserRepository $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function generateApiToken(User $user): User
    {
        do {
            $token = Hash::make(Str::random());
        } while ($this->repository->authTokenExist($token));
        $user->auth_token = $token;
        $user->save();
        return $user;
    }

    public function getUserForAuth(Request $request): ?User
    {
        if ($request->headers->has('Authorization')) {
            $authHeader = $request->headers->get('Authorization');
            $token = explode(' ', $authHeader);
            return $this->repository->getByAuthToken($token[1]);
        }
        return null;
    }

    /**
     * @throws \Throwable
     */
    public function sendTokenResetPassword(SendTokenRecoveryPasswordDTO $dto): string
    {
        /** @var User $user */
        $user = $this->repository->getByEmail($dto->email);
        $token = Str::random(32);
        $user->password_reset_token = $token;
        $user->saveOrFail();
        $user->sendPasswordResetNotification($token);
        return $token;
    }

    /**
     * @throws \Throwable
     */
    public function recoverPassword(RecoveryPasswordDTO $dto): bool
    {
        /** @var User $user */
        $user = $this->repository->getByEmail($dto->email);
        if ($user->password_reset_token && $user->password_reset_token == $dto->token) {
            $user->password_reset_token = null;
            $user->setPassword($dto->password);
            return $user->saveOrFail();
        }
        return false;
    }
}