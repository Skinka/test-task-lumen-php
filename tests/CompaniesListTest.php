<?php

use App\Models\Company;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Testing\DatabaseMigrations;

class CompaniesListTest extends TestCase
{
    use DatabaseMigrations;

    public function test_success()
    {
        /** @var User $user */
        $user = User::factory()
            ->count(1)
            ->create(['password' => Hash::make('123')])
            ->first();
        Company::factory()
            ->count(5)
            ->create(['user_id' => $user->id]);
        $this->actingAs($user)
            ->get('/api/user/companies')
            ->seeStatusCode(Response::HTTP_OK)
            ->seeJsonStructure(['*' => ['id', 'name', 'phone', 'description']]);
    }

    public function test_auth()
    {
        $this->get('/api/user/companies')
            ->seeStatusCode(Response::HTTP_UNAUTHORIZED);
    }
}
