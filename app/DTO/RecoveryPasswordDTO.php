<?php

namespace App\DTO;

class RecoveryPasswordDTO extends DTO
{
    public string $email;
    public string $token;
    public string $password;
}