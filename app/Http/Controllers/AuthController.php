<?php

namespace App\Http\Controllers;

use App\DTO\RecoveryPasswordDTO;
use App\DTO\RegisterDTO;
use App\DTO\SendTokenRecoveryPasswordDTO;
use App\DTO\SignInDTO;
use App\Http\Resources\UserResource;
use App\Repositories\UserRepository;
use App\Services\AuthService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class AuthController extends Controller
{
    private UserRepository $repository;
    private AuthService    $service;

    public function __construct(UserRepository $repository, AuthService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * Sign-in in system
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     * @throws ValidationException
     * @throws UnknownProperties
     */
    public function signIn(Request $request): JsonResponse
    {
        $this->validate($request, [
            'email'    => 'required|string|email|exists:users,email',
            'password' => 'required|string'
        ]);

        $dto = new SignInDTO($request->only(['email', 'password']));

        $user = $this->repository->getByEmail($dto->email);
        if ($user->checkPassword($dto->password)) {
            $user = $this->service->generateApiToken($user);
            return response()->json(['token' => $user->auth_token], Response::HTTP_OK);
        }
        return response()->json([
            "password" => [
                "The password field is incorrect."
            ]
        ], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Register user
     *
     * @throws UnknownProperties
     * @throws ValidationException
     * @throws \Throwable
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'email'      => 'required|string|email|unique:users,email',
            'first_name' => 'nullable|string',
            'last_name'  => 'nullable|string',
            'phone'      => 'nullable|string',
            'password'   => 'required|string'
        ]);

        $dto = new RegisterDTO($request->only([
            'email',
            'first_name',
            'last_name',
            'phone',
            'password'
        ]));

        $user = $this->repository->create($dto);
        return response()->json(new UserResource($user), Response::HTTP_CREATED);
    }

    /**
     * Send reset password token
     *
     * @throws ValidationException
     * @throws UnknownProperties
     * @throws \Throwable
     */
    public function sendRecoverPassword(Request $request): JsonResponse
    {
        $this->validate($request, [
            'email' => 'required|string|email|exists:users,email',
        ]);

        $dto = new SendTokenRecoveryPasswordDTO($request->only('email'));
        $this->service->sendTokenResetPassword($dto);
        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * Reset password
     *
     * @throws UnknownProperties
     * @throws ValidationException
     * @throws \Throwable
     */
    public function recoverPassword(Request $request): JsonResponse
    {
        $this->validate($request, [
            'email'                 => 'required|string|email|exists:users,email',
            'token'                 => 'required|string',
            'password'              => 'required|string|confirmed',
            'password_confirmation' => 'required|string',
        ]);

        $dto = new RecoveryPasswordDTO($request->only(['email', 'token', 'password']));
        if (!$this->service->recoverPassword($dto)) {
            return response()->json(['token' => ['Token invalid.']], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}