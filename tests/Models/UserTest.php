<?php

namespace Models;

use App\Models\Company;
use App\Models\User;
use Laravel\Lumen\Testing\DatabaseMigrations;

class UserTest extends \TestCase
{
    use DatabaseMigrations;

    public function testGetEmailForPasswordReset()
    {
        /** @var User $user */
        $user = User::factory()
            ->count(1)
            ->create()
            ->first();
        Company::factory()
            ->count(5)
            ->create(['user_id' => $user->id]);
        $this->assertCount(5, $user->companies);
    }

    public function testCompanies()
    {
        /** @var User $user */
        $user = User::factory()
            ->count(1)
            ->create()
            ->first();
        $this->assertEquals($user->email, $user->getEmailForPasswordReset());
    }
}
