<?php

use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Testing\DatabaseMigrations;

class AddCompanyTest extends TestCase
{
    use DatabaseMigrations;

    public function test_success()
    {
        /** @var User $user */
        $user = User::factory()
            ->count(1)
            ->create(['password' => Hash::make('123')])
            ->first();
        $this->actingAs($user)
            ->post('/api/user/companies', [
                'name'  => 'Test',
                'phone' => '+380961234567'
            ])
            ->seeStatusCode(Response::HTTP_CREATED)
            ->seeJsonEquals([
                'id'          => 1,
                'name'        => 'Test',
                'phone'       => '+380961234567',
                'description' => null
            ]);
    }

    public function test_auth()
    {
        $this->post('/api/user/companies', [
            'name' => 'Test'
        ])->seeStatusCode(Response::HTTP_UNAUTHORIZED);
    }
}
