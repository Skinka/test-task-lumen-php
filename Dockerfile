FROM php:fpm-alpine

RUN set -xe \
    && apk add --update icu && apk add --no-cache \
        autoconf make wget grep composer build-base zlib-dev icu-dev \
        zip unzip git coreutils libltdl libmcrypt-dev postgresql-dev libxml2-dev \
        curl libcurl supervisor

RUN docker-php-ext-configure intl \
    && docker-php-ext-install intl

RUN docker-php-ext-configure bcmath --enable-bcmath \
    && docker-php-ext-install bcmath

RUN docker-php-ext-configure pdo_pgsql \
    && docker-php-ext-install pdo pdo_pgsql

RUN pecl channel-update pecl.php.net
RUN pecl install mcrypt-1.0.4 && docker-php-ext-enable mcrypt
RUN pecl install xdebug-3.0.4 && docker-php-ext-enable xdebug

RUN chmod 777 /tmp
RUN mkdir /run/php-fpm && chmod 755 /run/php-fpm

COPY docker/php.ini /usr/local/etc/php/conf.d/50-setting.ini

WORKDIR /var/www

EXPOSE 9000
ENTRYPOINT ["php-fpm"]
