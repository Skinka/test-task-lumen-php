<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
// API
$router->group(['prefix' => 'api'], function () use ($router) {
    // User
    $router->group(['prefix' => 'user', 'as' => 'user.'], function () use ($router) {
        $router->post('sign-in', [
            'as'   => 'sign-in',
            'uses' => 'AuthController@signIn'
        ]);
        $router->post('register', [
            'as'   => 'register',
            'uses' => 'AuthController@register'
        ]);
        $router->patch('recover-password', [
            'uses' => 'AuthController@sendRecoverPassword',
        ]);
        $router->post('recover-password', [
            'uses' => 'AuthController@recoverPassword',
        ]);
        $router->get('companies', [
            'as'         => 'companies',
            'uses'       => 'UserController@companies',
            'middleware' => 'auth'
        ]);
        $router->post('companies', [
            'as'         => 'companies',
            'uses'       => 'UserController@newCompany',
            'middleware' => 'auth'
        ]);
    });
});
